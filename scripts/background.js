/**
*   
*   @author - devappdreams   
*   YOUTUBE LAYER ID
*   bnkfdgkccidbedkfoneaajldjgagjhib
*   UA-2592219-5
*
*
*/

(function(){

    var windowOnTop = false, 
    showAds, 
    hasExt, 
    isOnTop = false;

    function CreateVideo(webview, videoId) {
        if(!videoId) {
          return;
        }
        var url = 'http://www.youtube.com/embed/'+videoId+'?';
        url += '&showinfo=0';
        url += '&enablejsapi=1';
        url += '&version=3';
        url += '&autohide=1';
        url += '&fs=0';
        url += '&autoplay=1';
        url += '&hd=1';
        url += '&rel=0';
        webview.src = url;
    }

    function Aligments(appwindow, anchor) {
        switch(anchor) {
            case "top-left":
                appwindow.outerBounds.left = 0;
                appwindow.outerBounds.top = 0;
            break;
            case "top-right":
                appwindow.outerBounds.left = screen.width - appwindow.innerBounds.width;
                appwindow.outerBounds.top = 0;
            break;
            case "bottom-right":
                appwindow.outerBounds.left = screen.width - appwindow.innerBounds.width;
                appwindow.outerBounds.top = screen.height - appwindow.innerBounds.height;
            break;
            case "bottom-left":
                appwindow.outerBounds.left = 0;
                appwindow.outerBounds.top = screen.height - appwindow.innerBounds.height;
            break;
            case "center-center":
                appwindow.outerBounds.left = Math.round((screen.width * 0.5) - (appwindow.innerBounds.width * 0.5));
                appwindow.outerBounds.top = Math.round((screen.height * 0.5) - (appwindow.innerBounds.height * 0.5));
            break;
        }
    }

    function getLocalStorageValues(keys, callback) {
        chrome.storage.local.get(['left', 'top', 'ads', 'toogle'], function(saved) {
            //console.log("saved.left", saved.left);
            //console.log("saved.top", saved.top);
            //console.log("saved.top", saved.ads);
            //console.log("saved.alwaysOnTop", saved.alwaysOnTop);
            callback(saved);
        });
    }

    function RenderWindow(appwindow){
      
        appwindow.contentWindow.onload = function () {
            var opened = false;
            var focusInput = false;
            var inputCurrentValue;
            var webviewObj = appwindow.contentWindow.document.querySelector('#webview');
            var expandBtn = appwindow.contentWindow.document.querySelector('.expand-btn');
            var playBtn = appwindow.contentWindow.document.querySelector('.play-btn');
            var closeBtn = appwindow.contentWindow.document.querySelector('.close-btn');
            var moveBtn = appwindow.contentWindow.document.querySelector('.move-btn');
            var resizeBtn = appwindow.contentWindow.document.querySelector('.resize-btn');
            var tagSelect = appwindow.contentWindow.document.querySelector('.align');
            var inText = appwindow.contentWindow.document.querySelector('.input-text');
            var fullscreenBtn = appwindow.contentWindow.document.querySelector('.fullscreen-btn');
            var boxInfo = appwindow.contentWindow.document.querySelector('.box-info');
            var alwaysOnTopBtn = appwindow.contentWindow.document.querySelector('.always-on-top-btn');
            var alignOptions = appwindow.contentWindow.document.querySelector('#align-options');

            //updateAlwaysOnTop();

            inText.addEventListener('focus', function(e){
                focusInput = true;
                this.select();
            });

            inText.addEventListener('blur', function(e){
                focusInput = false;
            });

            alignOptions.addEventListener('click', function(e){
                return;
                var radVal = alignOptions.align.value;
                sendMessage(radVal);
                Aligments(appwindow, radVal);
                var boxAlign = appwindow.contentWindow.document.querySelector('.box-align');
                boxAlign.style.display = 'none';
            });

            alwaysOnTopBtn.addEventListener('click', function(e){
                /*if (!isOnTop) {
                    chrome.storage.local.set({ 'alwaysOnTop': true });
                    isOnTop = true;
                } else {
                    chrome.storage.local.set({ 'alwaysOnTop': false });
                    isOnTop = false;
                }*/
                updateAlwaysOnTop();
            });

            playBtn.addEventListener('click', function(e){
                playVideo();
            });

            expandBtn.addEventListener('click', function(e){
                toggleNav();
            });
            
            closeBtn.addEventListener('click', function(e){
                appwindow.contentWindow.trackAnalytics('closeBtn', 'close-app');
                appwindow.contentWindow.close();
            });

            moveBtn.addEventListener('click', function(e){
                appwindow.contentWindow.trackAnalytics('moveBtn', 'move-app');
                return false;
            });

            fullscreenBtn.addEventListener('click', function(e){
                toggleFullscreen();
            });

            appwindow.contentWindow.document.addEventListener('keydown', function(event) {
                if (event.keyCode === 13) { // shortcut to play video - 'enter' key
                    playVideo();
                }

                if (event.keyCode === 70) { // shortcut to fullscreen - 'f' key
                    if (focusInput === true) {
                        return;
                    }
                    if (opened === true){
                        toggleNav();
                    }
                    toggleFullscreen();
                }

                if (event.keyCode === 37) { // shortcut to move left - '<' left arrow
                    appwindow.outerBounds.left -= 10;
                }

                if (event.keyCode === 38) { // shortcut to move top - '^' up arrow
                    appwindow.outerBounds.top -= 10;
                }

                if (event.keyCode === 39) { // shortcut to move right - '>' right arrow
                    appwindow.outerBounds.left += 10;
                }

                if (event.keyCode === 40) { // shortcut to move down - 'v' down arrow
                    appwindow.outerBounds.top += 10;
                }

                if (event.keyCode === 107 || (event.keyCode === 187 && event.ctrlKey === true)) { // shortcut to resize - '+' plus
                    var width = appwindow.outerBounds.width + 32,
                    height = Math.round(width * (9 / 16));

                    appwindow.outerBounds.top -= 9;
                    appwindow.outerBounds.left -= 16;

                    appwindow.outerBounds.width = width;
                    appwindow.outerBounds.height = height;
                }

                if (event.keyCode === 109 || (event.keyCode === 189 && event.ctrlKey === true)) { // shortcut to resize - '-' minus
                    var width = appwindow.outerBounds.width - 32,
                    height = Math.round(width * (9 / 16));

                    if (appwindow.outerBounds.width > appwindow.outerBounds.minWidth) {
                        appwindow.outerBounds.top += 9;
                        appwindow.outerBounds.left += 16;
                    }

                    appwindow.outerBounds.width = width;
                    appwindow.outerBounds.height = height;
                }

                if(event.ctrlKey === true && event.keyCode === 78) {
                    toggleNav();
                }

            }, true);

            function updateAlwaysOnTop() {
                //if (windowOnTop) {
                if (appwindow.isAlwaysOnTop()) {
                    alwaysOnTopBtn.style.backgroundPosition =  '-202px 0px';
                    appwindow.contentWindow.trackAnalytics('setAlwaysOnTop', 'false');
                    appwindow.setAlwaysOnTop(false);
                } else {
                    alwaysOnTopBtn.style.backgroundPosition =  '-177px 0px';
                    appwindow.contentWindow.trackAnalytics('setAlwaysOnTop', 'true');
                    appwindow.setAlwaysOnTop(true);
                }
            }

            function playVideo() {
                var video;
                
                if(!inText.value) {
                    appwindow.contentWindow.trackAnalytics('playBtn', 'input-text-empty');
                    appwindow.contentWindow.document.querySelector('#info').textContent = 'Enter a URL video';
                    appwindow.contentWindow.dialog.showModal();
                    return;
                } 
                
                var valid = inText.value.match(/http(s)?|www|youtube|watch\?v=/g);
                
                if(!(valid instanceof Array) || valid.length === 0) {
                    appwindow.contentWindow.trackAnalytics('playBtn', 'input-text-invalid');
                    appwindow.contentWindow.document.querySelector('#info').textContent = 'Enter a URL or video ID valid';
                    appwindow.contentWindow.dialog.showModal();
                    return;
                }

                if (inText.value.indexOf("watch") !== -1 ) {
                    video = inText.value.split('watch?v=')[1];
                } else {
                    video = inText.value;
                }

                appwindow.contentWindow.trackAnalytics('playBtn', video);
                toggleNav();
                CreateVideo(webviewObj, video);
            }

            function sendMessage(message) {
                var info = appwindow.contentWindow.document.querySelector('#alert');
                info.innerHTML = message;
            }

            function toggleNav() {
                var header = appwindow.contentWindow.document.querySelector('.nav-menu');
                if (header.style.top === '' || header.style.top === '-50px') {
                    appwindow.contentWindow.trackAnalytics('toggleNav', 'open');
                    header.style.top = '0px';
                    opened = true;
                    inText.focus();
                    inText.select();
                } else {
                    appwindow.contentWindow.trackAnalytics('toggleNav', 'close');
                    header.style.top = '-50px';
                    opened = false;
                    inText.blur();
                }
                header.style.opacity = (header.style.opacity === "1") ? "0.4" : "1";
                expandBtn.style.background = (expandBtn.style.background === "rgb(255, 255, 255)") ? "rgb(68, 68, 68)" : "rgb(255, 255, 255)";
            }

            function toggleFullscreen() {
                if (appwindow.isFullscreen()) {
                    fullscreenBtn.style.backgroundPosition =  '-101px 0px';
                    appwindow.contentWindow.trackAnalytics('toggleFullscreen', 'restore');
                    appwindow.restore();
                } else {
                    fullscreenBtn.style.backgroundPosition =  '-152px 0px';
                    appwindow.contentWindow.trackAnalytics('toggleFullscreen', 'fullscreen');
                    appwindow.fullscreen();
                }
            };

            setTimeout(function(){
                toggleNav();
            }, 1000);
        };
    }


    function CreateWindow () {
        var options = {
            'frame': 'none', //none //chrome
            //transparentBackground: true, 
            'id': 'YoutubeLayer',
            'minWidth': 320,
            'minHeight': 240,
            'alwaysOnTop': true,
            'resizable': true,
            'bounds': {
                'width' : 320, 
                'height': 240,
                'left' : Math.round(window.screen.availWidth*0.5-320*0.5),
                'top'  : Math.round(window.screen.availHeight*0.5-240*0.5)
            }
        };
        chrome.app.window.create('index.html', options,  RenderWindow);
    }

    chrome.storage.onChanged.addListener(function(changes, namespace) {
        var appwindow = chrome.app.window.getAll()[0];
        console.log("if",appwindow);
        for (key in changes) {
            var storageChange = changes[key];
            if (key === 'alwaysOnTop') {
                return;
                //windowOnTop = storageChange.newValue;
                var onTopBtn = appwindow.contentWindow.document.querySelector('.always-on-top-btn');
                if (!windowOnTop) {
                    onTopBtn.style.backgroundPosition =  '-202px 0px';
                    appwindow.contentWindow.trackAnalytics('setAlwaysOnTop', 'false');
                    appwindow.setAlwaysOnTop(false);
                } else {
                    onTopBtn.style.backgroundPosition =  '-177px 0px';
                    appwindow.contentWindow.trackAnalytics('setAlwaysOnTop', 'true');
                    appwindow.setAlwaysOnTop(true);
                }
            }
        }

    });

    chrome.runtime.onInstalled.addListener(function (details) {
        if (details.reason === 'install') {
            chrome.storage.local.set({'alwaysOnTop': true});
        } else if (details.reason === 'update') {
            //
        }
    });

    chrome.app.runtime.onLaunched.addListener(function() {
        chrome.storage.local.get('extension', function (saved) {
            if (typeof saved.extension === 'undefined') {
                chrome.storage.local.set({ 'extension': false });
            } else {
                hasExt = saved.extension;
            }
        });
        chrome.storage.local.get('ads', function (saved) {
            if (typeof saved.ads === 'undefined') {
                chrome.storage.local.set({ 'ads': true });
            } else {
                showAds = saved.ads;
            }
        });
        chrome.storage.local.get('alwaysOnTop', function (saved) {
            //windowOnTop = saved.alwaysOnTop;
        });
        CreateWindow();
    });
})();