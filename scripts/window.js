/**
*   
*   @author - devappdreams   
*   YOUTUBE LAYER ID
*   bnkfdgkccidbedkfoneaajldjgagjhib
*   UA-2592219-5
*
*
*/

(function(window){
    var tracker, webViewElement;
    webViewElement = document.getElementById('webview');
    webViewElement.addEventListener('contentload', function () {
        webViewElement.contentWindow.postMessage('Send appWindow reference', "*");
    });
    window.trackAnalytics = function (value, desc) {
        desc = (desc === undefined) ? "" : desc;
        tracker.sendEvent(value, desc);
    };
    function startApp() {
        var service = analytics.getService('ice_cream_app');
        tracker = service.getTracker('UA-2592219-5');
        tracker.sendAppView('init-app');
    }
    var dialog = document.querySelector('#dialog');
    var closeBtn = document.querySelector('#close');
    closeBtn.addEventListener("click", function(e) {
        dialog.close("thanks!");
    });
    startApp();
})(window);