function xhrWithAuth(method, url, interactive, callback) {
    try {
      chrome.identity.getAuthToken({interactive: interactive}, function(token) {
          if (token) {
            this.accessToken = token;
            callback && callback();
          }
      }.bind(this));
    } catch(e) {
      console.log(e);
    }
  }